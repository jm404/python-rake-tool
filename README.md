# python-rake-tool
Personal python-rake tool implementation to analyze language and use of words.

# Objetives

The strength of rake is the hability to quickly analyze text and display results using basic statistical data.
My goal here is develop a personal script to analyze multiple texts and extract top 3-5 used words to search patterns and extract high-valuable information.

# Basic Usage

The tool provides a simple menu too choose preferences including regex.

# TODO's

+ Add minium number of concurrences filter [OK]
+ Add export results to csv and txt
+ Add multiple files support 
+ Add arguments : language, minimum concurrences, max length, etc... [OK]

# Author

Jose Miguel García Ramos. Granada, 2019.





