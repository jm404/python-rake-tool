import RAKE 
import sys
import 


def main(str_args):

    print ("""
     Choose the list of stopwords
        1. English
        2. Spanish
        3. Fox
        4. Google
        5. MySQL
        6. NLTK
        7. RanksNLLong
        8. RanksNL
        9. Smart
    """)
    stopwords_selection = input("Selection: ")

    print ("Path of the txt file: ")
    path_of_file_selection = input("")

    print ("Regex expression:")
    regex_selection = input("(leave empty for default): ")
    
    print ("Min. Characters:")
    min_chars_selection = int(input("(leave empty for default): "))

    print ("Max. Words:")
    max_words_selection = int(input("(leave empty for default): "))

    print ("Min. Frecuency:")
    min_frecuency_selection = int(input("(leave empty for default): "))

    ## Processing selection

    # Validating modifiers

    if (stopwords_selection == ""):
        print ("ERROR. A list of stopwards must be choose.")
        sys.exit()

    if (path_of_file_selection == ""):
        print ("ERROR. Path can not be empty.")
        sys.exit()

    if (regex_selection == ""):
        regex_selection = r"[\W\n]+"
    
    if (min_chars_selection is None):
        min_chars_selection = 1
    if (max_words_selection is None):
        max_words_selection = 5
    if (min_frecuency_selection is None ):
        min_frecuency_selection = 1

    # Selecting Stopwords list 

    if (stopwords_selection == "1"):
        
    elif (stopwords_selection == "2"):

    elif (stopwords_selection == "3"):
        rake = RAKE.(RAKE.FoxStopList.words())
    elif (stopwords_selection == "4"):
        rake = RAKE.(RAKE.GoogleSearchStopList.words())
    elif (stopwords_selection == "5"):
        rake = RAKE.(RAKE.MySQLStopList.words())
    elif (stopwords_selection == "6"):
        rake = RAKE.(RAKE.NLTKStopList.words())
    elif (stopwords_selection == "7"):
        rake = RAKE.(RAKE.RanksNLLongStopList.words())
    elif (stopwords_selection == "8"):
        rake = RAKE.(RAKE.RanksNLStopList.words())
    elif (stopwords_selection == "9"):
        rake = RAKE.(RAKE.SmartStopList.words())

    # in_file = open(arg_string, "rt")
    # text = in_file.read()
    # in_file.close()

    # result = Rake.run(text, minCharacters = 3, maxWords = 4, minFrequency = 1)

    # for word in result:
    #     print(word)

    
def get_es_stoplist():
    list_stopwords = open("es_stopwords.txt").readlines()
    list_prepositions = open("es_prepositions.txt").readlines()
    return list_stopwords + list_prepositions

def get_en_stoplist():
    list_stopwords = open("es_stopwords.txt").readlines()
    list_prepositions = open("es_prepositions.txt").readlines()
    return list_stopwords + list_prepositions

if __name__ == "__main__":
    try:
        arg_string = str(sys.argv[1])
    except:
        print ('Error, argument not valid')
        sys.exit()

    main(arg_string)